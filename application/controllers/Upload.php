<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {
	
	public function construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('M_HandleFile');
	}

	public function index()
	{
		$this->load->model('M_HandleFile');
		$model = $this->M_HandleFile;
		$file = $model->get_data();
		$data = [
			'file' => $file,
			'error' => '',
		];
		$this->load->view('upload', $data);
	}
	public function upload_file() {
		// Upload image
		$config['upload_path'] = 'public/uploads/';
		$config['allowed_types'] = 'png|jpg|jpeg|gif';
		$config['max_size'] = 5000; // in KB (5MB)
	
		$this->load->library('upload', $config);
	
		if (!$this->upload->do_upload('imageFile')) {
			$error = array('error' => $this->upload->display_errors());
			$this->load->view('upload', $error);
		} else {
			// Upload PDFs
			$pdfFiles = $_FILES['pdfFiles'];
			$uploaded_files_data = array();
	
			// Menyimpan data gambar
			$uploaded_image_data = $this->upload->data();
			$image_data = array(
				'path' => 'public/uploads/' . $uploaded_image_data['file_name'],
				'file_type' => $uploaded_image_data['file_type']
			);
			array_push($uploaded_files_data, $image_data);
	
			foreach ($pdfFiles['name'] as $key => $pdfFile) {
				$_FILES['pdfFile']['name'] = $pdfFiles['name'][$key];
				$_FILES['pdfFile']['type'] = $pdfFiles['type'][$key];
				$_FILES['pdfFile']['tmp_name'] = $pdfFiles['tmp_name'][$key];
				$_FILES['pdfFile']['error'] = $pdfFiles['error'][$key];
				$_FILES['pdfFile']['size'] = $pdfFiles['size'][$key];
	
				$config['upload_path'] = 'public/uploads/';
				$config['allowed_types'] = 'pdf';
				$config['max_size'] = 5000; // in KB (5MB)
	
				$this->upload->initialize($config);
	
				if ($this->upload->do_upload('pdfFile')) {
					$uploaded_data = $this->upload->data();
					$file_data = array(
						'path' => 'public/uploads/' . $uploaded_data['file_name'],
						'file_type' => $uploaded_data['file_type']
					);
					array_push($uploaded_files_data, $file_data);
				}
			}
	
			// Masukkan data ke database
			if (!empty($uploaded_files_data)) {
				// Load model
				$this->load->model('M_HandleFile'); // Sesuaikan dengan nama model Anda
	
				// Memanggil metode insert_files dari model
				$inserted_files = $this->M_HandleFile->insert_files($uploaded_files_data);
	
				if (!empty($inserted_files)) {
					$data = array('upload_data' => $inserted_files);
					$this->session->set_flashdata('success', 'File berhasil diupload');
					redirect('upload');
				} else {
					$error = array('error' => 'Gagal memasukkan data ke database');
						redirect('upload');
				}
			} else {
				$error = array('error' => 'Gagal mengunggah file PDF');
					redirect('upload');
			}
		}
	}


	public function delete($fileId)
{
    // Mendapatkan path file berdasarkan ID
    $fileData = $this->db->where('id', $fileId)->get('upload_file')->row();
    $filePath = $fileData->path;

    // Menghapus file dari sistem file
    if (file_exists($filePath)) {
        unlink($filePath);
    }

    // Menghapus data dari database
    $this->db->where('id', $fileId);
    $this->db->delete('upload_file');

    // Set pesan sukses untuk ditampilkan di halaman index
    $this->session->set_flashdata('success', 'File berhasil dihapus');

    // Redirect kembali ke method index di controller Upload
	redirect('upload');
}


	
	
}
