<?php


class M_HandleFile extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	public function get_data() {
        $query = $this->db->get('upload_file');
        return $query->result_array();
    }
	public function insert_files($uploaded_files_data) {
		$inserted_files = [];
		foreach ($uploaded_files_data as $file_data) {
			$data = array(
				'path' => $file_data['path'],
				'file_type' => $file_data['file_type']
			);
	
			// Masukkan data ke dalam tabel 'upload_file'
			$this->db->insert('upload_file', $data);
	
			// Periksa apakah data berhasil dimasukkan
			if ($this->db->affected_rows() > 0) {
				array_push($inserted_files, $data);
			}
		}
	
		return $inserted_files;
	}
	
}	
