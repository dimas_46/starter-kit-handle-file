<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
	<title>Document</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Handle File</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Link</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Dropdown
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="#">Action</a></li>
            <li><a class="dropdown-item" href="#">Another action</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">Something else here</a></li>
          </ul>
        </li>
        <li class="nav-item">
          <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
        </li>
      </ul>
      <form class="d-flex">
        <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success" type="submit">Search</button>
      </form>
    </div>
  </div>
</nav>
<div class="container mt-5">
	<div class="card">
		<div class="card-header">
		<h4>Arkatama Starterkit: Handle File upload</h4>
		<?php if ($this->session->flashdata('success')): ?>
			<div class="alert alert-success alert-dismissible fade show" role="alert">
				<?= $this->session->flashdata('success') ?>
				<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
			</div>
			<?php endif; ?>
		
		</div>
		<div class="card-body">
		<form action="<?= base_url('upload/upload_file')?>" method="post" enctype="multipart/form-data">
		<div class="mb-3">
			<label for="imageFile">Upload Image</label><span class="text-danger ms-1">*</span>
			<input type="file" class="form-control" name="imageFile" accept=".png, .jpg, .jpeg, .jpg" required>
		</div>
		<div class="mb-3">
			<label for="pdfFiles">Upload Pdf (Multiple)</label><span class="text-danger ms-1">*</span>
			<input type="file" class="form-control" name="pdfFiles[]" accept=".pdf" multiple required>
		</div>
		<button type="submit" class="btn btn-primary">Submit</button>
	</form>

		</div>
		<div class="card-body">
			<table class="table table-bordered">
				<thead>
					<th>No</th>
					<th>Path</th>
					<th>Type</th>
					<th>Aksi</th>
				</thead>
				<tbody>
				<?php 
					$key = 1; // Mulai dari angka 1
					foreach ($file as $data): 
					?>
					<tr>
						<td><?= $key++ ?></td>
						<td><?= $data['path'] ?></td>
						<td><?= substr($data['file_type'], strpos($data['file_type'], '/') + 1) ?></td>
						<td><a href="#" data-bs-toggle="modal" data-bs-target="#deleteModal<?= $data['id'] ?>" class="btn btn-danger btn-sm">Delete</a></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>



</body>
</html>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
<?php 
foreach ($file as $data): ?>
<div class="modal fade" id="deleteModal<?= $data['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
		Apakah anda yakin ingin menghapus data ini?
		<form action="<?= base_url('upload/delete/'.$data['id'])?>" method="post">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-danger">Hapus</button>
      </div>
	  </form>
    </div>
  </div>
</div>
<?php endforeach; ?>
